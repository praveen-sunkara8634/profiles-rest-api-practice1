from django.apps import AppConfig


class Practice1ApiConfig(AppConfig):
    name = 'practice1_api'
